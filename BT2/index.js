function tinhTienDien() {
    var tenNguoiSuDung = document.getElementById("ten-nguoi-su-dung").value ;
    var soKw = document.getElementById("so-kw").value * 1;

    var giaTien50KwDau = 500;
    var giaTien50KwDen100Kw = 650;
    var giaTien100KwDen200Kw = 850;
    var giaTien200KwDen350Kw = 1100;
    var giaTien350KwTroDi = 1300;

    var tongTien = 0;
    if (soKw <= 50) {
        tongTien = soKw * giaTien50KwDau;
    } else if (soKw <= 100) {
        tongTien = 50 * giaTien50KwDau + (soKw - 50) * giaTien50KwDen100Kw;
    } else if (soKw <= 200) {
        tongTien = 50 * giaTien50KwDau + 50 * giaTien50KwDen100Kw + (soKw - 100) * giaTien100KwDen200Kw;
    }else if (soKw <= 350) {
        tongTien = 50 * giaTien50KwDau + 50 * giaTien50KwDen100Kw + 100 * giaTien100KwDen200Kw + (soKw - 200) * giaTien200KwDen350Kw;
    } else {
        tongTien = 50 * giaTien50KwDau + 50 * giaTien50KwDen100Kw + 100 * giaTien100KwDen200Kw + 150 * giaTien200KwDen350Kw + (soKw - 350) * giaTien350KwTroDi;
    }

    document.getElementById("result").innerHTML = `${"Họ tên: " + tenNguoiSuDung+ ", Tổng tiền điện: " + tongTien.toLocaleString() + " VND"}`
    

}