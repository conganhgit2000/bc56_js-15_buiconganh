function tinhTienThue() {
    var hoTen = document.getElementById('ho-ten').value ;
    var tongThuNhapNam = document.getElementById('tong-thu-nhap-nam').value * 1;
    var soNguoiPhuThuoc = document.getElementById('so-nguoi-phu-thuoc').value * 1;
    
    var thuNhapChiuThue = tongThuNhapNam - 4000000 - soNguoiPhuThuoc * 1600000;

    var tongTien = 0;
    if (thuNhapChiuThue <= 60000000) {
        tongTien = thuNhapChiuThue * 0.05;
    } else if (thuNhapChiuThue <= 120000000) {
        tongTien = thuNhapChiuThue * 0.1;
    } else if (thuNhapChiuThue <= 210000000) {
        tongTien = thuNhapChiuThue * 0.15;
    } else if (thuNhapChiuThue <= 384000000) {
        tongTien = thuNhapChiuThue * 0.2;
    } else if (thuNhapChiuThue <= 624000000) {
        tongTien = thuNhapChiuThue * 0.25;
    } else if (thuNhapChiuThue <= 960000000) {
        tongTien = thuNhapChiuThue * 0.3;
    } else {
        tongTien = thuNhapChiuThue * 0.35;
    }

    document.getElementById("result").innerHTML = `${"Họ tên: " + hoTen + ", Tiền thuế thu nhập cá nhân: " + tongTien.toLocaleString() + " VND"}`
}