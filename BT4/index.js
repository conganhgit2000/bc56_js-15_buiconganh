
function phiXuLyHoaDon(khachHang) {
    if (khachHang == "nhaDan") {
        return 4.5;
    }
    if (khachHang == "doanhNghiep") {
        return 15;
    }
}
function phiDichVuCoBan(khachHang) {
    if (khachHang == "nhaDan") {
        return 20.5;
    }
    if (khachHang == "doanhNghiep") {
        return 75;
    }
}
function thueKenhCaoCap(khachHang) {
    if (khachHang == "nhaDan") {
        return 7.5;
    }
    if (khachHang == "doanhNghiep") {
        return 50;
    }
}

function anHienInput() {
    var khachHang = document.getElementById('loai-khach-hang').value ;
    if (khachHang == "doanhNghiep") {
        document.getElementById('so-ket-noi').style.display = "block";
    } else {
        document.getElementById('so-ket-noi').style.display = "none";
    }
}

function tinhTienCap() {
    var khachHang = document.getElementById('loai-khach-hang').value ;
    var maKhachHang = document.getElementById('ma-khach-hang').value;
    var soKenhCaoCap = document.getElementById('so-kenh-cao-cap').value * 1;
    var soKetNoi = document.getElementById('so-ket-noi').value * 1;

    // lấy  giá trị của từng loại khách hàng
    var giaTienXuLyHoaDon = phiXuLyHoaDon(khachHang);
    var giaTienDichVuCoBan = phiDichVuCoBan(khachHang);
    var giaTienThueKenhCaoCap = thueKenhCaoCap(khachHang);

    // tính tiền 
    var tongTien = 0;


    if (khachHang == "nhaDan") {
        tongTien = giaTienXuLyHoaDon + giaTienDichVuCoBan + giaTienThueKenhCaoCap * soKenhCaoCap;
    }
    if (khachHang == "doanhNghiep") {
        if (soKetNoi <= 10) {
            tongTien = giaTienXuLyHoaDon + giaTienDichVuCoBan + giaTienThueKenhCaoCap * soKenhCaoCap;
        } else {
            tongTien = giaTienXuLyHoaDon + giaTienDichVuCoBan + (soKetNoi - 10) * 5 + giaTienThueKenhCaoCap * soKenhCaoCap;
        };
    }
    document.getElementById("result").innerHTML = `Mã khách hàng: ${maKhachHang} , Tiền cáp: ${tongTien.toLocaleString()} $`;

}